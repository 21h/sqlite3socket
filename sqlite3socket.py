# Copyright by Vladimir Smagin, 2016
# http://blindage.org
# 21h@blindage.org
# Project page: http://dev.blindage.org/projects/sqlite3socket

# Licensed under GPL.

import socketserver, socket, os, sys
import sqlite3
import json

# configuration

confDB = "test.db"
# write here "socketfile" to use unix file socket insted binding to network interface
# using socket file makes all faster!
confBinTo = "socketfile"
confBindAddr = "127.0.0.1"
confBindPort = 5566
condBindSocketFile = "/tmp/sqlite3.sock"

class betterSocket(socketserver.TCPServer):
    address_family = socket.AF_UNIX


class DB:
    def __init__(self, confDB):
        self.db = sqlite3.connect(confDB)

    def fetchall(self, query):
        dbcursor = self.db.cursor()
        dbcursor.execute(query)
        self.db.commit()
        return dbcursor.fetchall()


class s3socket(socketserver.BaseRequestHandler):
    def handle(self):
        self.data = self.request.recv(1024).strip()
        try:
            sqlresult = dbHandler.fetchall(self.data.decode('UTF-8'))
            answer = ['result', sqlresult]
        except sqlite3.Error as er:
            answer = ['error', er.args[0]]

        self.request.sendall(bytes(json.dumps(answer, ensure_ascii=False), 'UTF-8'))

print('Sqlite3 database server.')
try:
    dbHandler = DB(confDB)
    if confBinTo=='socketfile':
        print('Using unix socket file {}'.format(condBindSocketFile))
        try:
            os.remove(condBindSocketFile)
        except:
            pass
        server = betterSocket(condBindSocketFile, s3socket)
    else:
        print('Using bind to network interface {}:{}'.format(confBindAddr,confBindPort))
        server = socketserver.TCPServer((confBindAddr,confBindPort), s3socket)
    server.serve_forever()
except OSError:
    print('Exiting with error state')
    sys.exit(1)
