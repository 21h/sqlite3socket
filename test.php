<?php

$socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
if ($socket === false) {
    echo "socket_create() failed: reason: " . socket_strerror(socket_last_error()) . "\n";
} 

$result = socket_connect($socket, '127.0.0.1', 5566);

if ($result === false) {
    echo "socket_connect() failed.\nReason: ($result) " . socket_strerror(socket_last_error($socket)) . "\n";
}

$sql = 'select value from options where option="transmitter";';
$result_json = '';

socket_write($socket, $sql, strlen($sql));
while ($read = socket_read($socket, 2048)) {
    $result_json .= $read;
}
socket_close($socket);
var_dump(json_decode($result_json));